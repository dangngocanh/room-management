package com.anhdn.roommanagement.controller;

import com.anhdn.roommanagement.common.ExcelWriter;
import com.anhdn.roommanagement.entity.UserEntity;
import com.anhdn.roommanagement.model.request.UserRequest;
import com.anhdn.roommanagement.model.respone.ResponeData;
import com.anhdn.roommanagement.model.respone.ResponeDataPage;
import com.anhdn.roommanagement.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @PostMapping("upsert")
    public ResponeData upsert(@RequestBody UserRequest userRequestBody){
        UserEntity userEntity = userService.upsert(userRequestBody);
        return new ResponeData("200","Success", userEntity.getId());
    }

    @PostMapping("/search")
    public ResponeDataPage search(@RequestBody UserRequest userRequestbody){
        logger.error("Hello baby ---> {}", userRequestbody);
        Page<UserEntity> userEntityPage = userService.searchListUsername(userRequestbody);
        return new ResponeDataPage("200"," Success",
                userEntityPage.getTotalElements(),
                userEntityPage.getContent(),
                userRequestbody.getPage(),
                userRequestbody.getPageSize());
    }

    @DeleteMapping("/delete/{id}")
    public ResponeData deleteById(@PathVariable Long id){
        Boolean checkDelete = userService.deleteById(id);
        if(!checkDelete){
            throw new RuntimeException("delete Failed not found User");
        }
        return new ResponeData("200","Delete success", checkDelete);
    }


    @GetMapping("/export/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<UserEntity> listUsers = userService.findAll();

        ExcelWriter excelExporter = new ExcelWriter(listUsers);
        excelExporter.export(response);
    }

}
