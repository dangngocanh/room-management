package com.anhdn.roommanagement.controller;

import com.anhdn.roommanagement.exception.*;
import com.anhdn.roommanagement.entity.CustomerEntity;
import com.anhdn.roommanagement.model.request.CustomerRequest;
import com.anhdn.roommanagement.model.respone.ResponeData;
import com.anhdn.roommanagement.model.respone.ResponeDataPage;
import com.anhdn.roommanagement.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping("/upsert")
    public ResponeData upsert(@RequestBody CustomerRequest customerRequestBody){
        CustomerEntity customerEntity = customerService.upsert(customerRequestBody);
        return new ResponeData("200","ok",customerEntity.getId());
    }

    @PostMapping("/search")
    public ResponeDataPage search(@RequestBody CustomerRequest customerRequestBody){
        if(customerRequestBody.getPage() == 1){
            throw new CustomeException("001");
        }
        Page<CustomerEntity> customerEntityPage = customerService.search(customerRequestBody);
        return new ResponeDataPage("200","ok",
                customerEntityPage.getTotalElements(),
                customerEntityPage.getContent(),
                customerRequestBody.getPage(),
                customerRequestBody.getPage());
    }

    @DeleteMapping("/delete/{id}")
    public ResponeData deleteById(@PathVariable Long id){
        Boolean checkDelete = customerService.deleteById(id);
        if(!checkDelete){
            throw new RuntimeException("Delete Failed not found Customer");
        }
        return new ResponeData("200","ok", checkDelete);
    }

    @GetMapping("/getAllCustomer")
    public List getAllCustomer(){
        return customerService.getListCustomer();
    }
}
