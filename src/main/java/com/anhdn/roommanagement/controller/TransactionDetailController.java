package com.anhdn.roommanagement.controller;

import com.anhdn.roommanagement.entity.TransactionDetailEntity;
import com.anhdn.roommanagement.model.request.TransactionDetailRequest;
import com.anhdn.roommanagement.model.respone.ResponeData;
import com.anhdn.roommanagement.service.TransactionDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/transaction-detail")
public class TransactionDetailController {

    @Autowired
    private TransactionDetailService transactionDetailService;

    @PostMapping("/upsert")
    public ResponeData upsert(@RequestBody TransactionDetailRequest transactionDetailRequest){
        TransactionDetailEntity transactionDetailEntity = transactionDetailService.upsert(transactionDetailRequest);
        return new ResponeData("200","success",transactionDetailEntity.getId());
    }
}
