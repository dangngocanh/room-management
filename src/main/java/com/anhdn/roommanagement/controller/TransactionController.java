package com.anhdn.roommanagement.controller;

import com.anhdn.roommanagement.entity.TransactionEntity;
import com.anhdn.roommanagement.model.request.ReportRequest;
import com.anhdn.roommanagement.model.request.TransactionRequest;
import com.anhdn.roommanagement.model.respone.ResponeData;
import com.anhdn.roommanagement.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping("/upsert")
    public ResponeData upsert(@RequestBody TransactionRequest transactionRequest){
        TransactionEntity transactionEntity = transactionService.upsert(transactionRequest);
        return new ResponeData("200","ok",transactionEntity.getId());
    }

    @DeleteMapping("/delete/{id}")
    public ResponeData deleteById(@PathVariable Long id){
        Boolean checkDelete = transactionService.deleteById(id);
        if(!checkDelete){
            throw new RuntimeException("delete Failed not found Room");
        }
        return new ResponeData("200","Delete success", checkDelete);
    }

    @PostMapping("/report")
    public ResponeData report(@RequestBody ReportRequest reportRequest){
        transactionService.finDateBetween(reportRequest);
        return null;

    }

}
