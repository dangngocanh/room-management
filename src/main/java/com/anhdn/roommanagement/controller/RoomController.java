package com.anhdn.roommanagement.controller;

import com.anhdn.roommanagement.entity.RoomEntity;
import com.anhdn.roommanagement.model.request.RoomRequest;
import com.anhdn.roommanagement.model.respone.ResponeData;
import com.anhdn.roommanagement.model.respone.ResponeDataPage;
import com.anhdn.roommanagement.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/room")

public class RoomController {

    @Autowired
    private RoomService roomService;

    @PostMapping("/upsert")
    public ResponeData upsert(@RequestBody RoomRequest roomRequestBody){
        RoomEntity roomEntity = roomService.upsert(roomRequestBody);
        return new ResponeData("200", "Abc",roomEntity.getId());
    }

    @PostMapping("/search")
    public ResponeDataPage search(@RequestBody RoomRequest roomRequestBody){
        Page<RoomEntity> roomEntityPage = roomService.searchRoom(roomRequestBody);
        return new ResponeDataPage("200","",roomEntityPage.getTotalElements(),
                roomEntityPage.getContent(),
                roomRequestBody.getPage(),
                roomRequestBody.getPageSize()
        );
    }
    @DeleteMapping("/delete/{id}")
    public ResponeData deleteById(@PathVariable Long id){
        Boolean checkDelete = roomService.deleteId(id);
        if(!checkDelete){
            throw new RuntimeException("delete Failed not found Room");
        }
        return new ResponeData("200","Delete success", checkDelete);
    }

    @PostMapping("/check-room")
    public ResponeDataPage checkRoom(@RequestBody RoomRequest roomRequestBody){
        Page<RoomEntity> roomEntityPage = roomService.checkRoom(roomRequestBody);
        return new ResponeDataPage("200","ok",roomEntityPage.getTotalElements(),
                roomEntityPage.getContent(),
                roomRequestBody.getPage(),
                roomRequestBody.getPageSize()
        );
    }

}
