package com.anhdn.roommanagement.schedule;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ScheduleTask {
    private Logger logger = LoggerFactory.getLogger(ScheduleTask.class);



    @Scheduled(fixedRate = 2000) // 2000ms => 2s
    //FixedRate chạy lặp lại liên tục
    public void ScheduleStatusEmailFixedRate() {
//            logger.info("Scheduled    --> run ");

    }
}
