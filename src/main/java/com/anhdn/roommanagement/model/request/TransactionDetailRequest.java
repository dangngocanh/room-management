package com.anhdn.roommanagement.model.request;

public class TransactionDetailRequest {

    private Long id;
    private String transactionId;
    private String electricNumber;
    private String waterNumber;
    private int page;
    private int pageSize;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getElectricNumber() {
        return electricNumber;
    }

    public void setElectricNumber(String electricNumber) {
        this.electricNumber = electricNumber;
    }

    public String getWaterNumber() {
        return waterNumber;
    }

    public void setWaterNumber(String waterNumber) {
        this.waterNumber = waterNumber;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
