package com.anhdn.roommanagement.model.request;

import java.util.Date;

public class ReportRequest {
    private String dateTo;
    private String dateEnd;

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }
}
