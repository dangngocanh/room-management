package com.anhdn.roommanagement.model.respone;

public class ResponeDataPage {
    private String status;
    private String msg;
    private Long total;
    private Object data;
    private int page;
    private int pageSize;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public ResponeDataPage(String status, String msg, Long total, Object data, int page, int pageSize) {
        this.status = status;
        this.msg = msg;
        this.total = total;
        this.data = data;
        this.page = page;
        this.pageSize = pageSize;
    }
}
