package com.anhdn.roommanagement.common;

import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CommonUtil {

    private static Logger LOGGER = LoggerFactory.getLogger(CommonUtil.class);


    public static List<T> exportExcel(Map params, String templateName, String fileName){
        ByteArrayOutputStream byteArrayOutputStream = null;
        InputStream is = null;
        FileOutputStream fos = null;
        InputStreamResource resource = null;
        File file = null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            is = CommonUtil.getInputStreamByFileName(templateName);
            XLSTransformer transformer = new XLSTransformer();
            org.apache.poi.ss.usermodel.Workbook rsWorkbook = transformer.transformXLS(is, params);
            rsWorkbook.write(byteArrayOutputStream);
            try {
                String fullPathFile = CommonUtil.getFullPathFileNameReport(fileName);
                fos = new FileOutputStream(fullPathFile);
                fos.write(byteArrayOutputStream.toByteArray());
                file = new File(fullPathFile);
            } catch (IOException e) {
                return null;
            }finally {
                if (fos != null) fos.close();
            }
//            byte[] data = FileUtils.readFileToByteArray(file);
//            resource = new InputStreamResource(new FileInputStream(file));
        } catch (IOException | InvalidFormatException e) {
            return null;
        } finally {
            try {
                if (is != null) is.close();
                if (byteArrayOutputStream != null) byteArrayOutputStream.close();
            } catch (IOException ex) {
                return null;
            }
        }
//        List<InvFileAttachDTO> lstResult = new ArrayList<InvFileAttachDTO>();
//        InvFileAttachDTO inspFileAttachDtoTemp;
//        try {
//            String encoded = new String(org.apache.commons.codec.binary.Base64.encodeBase64(FileUtils.readFileToByteArray(file)));
//            inspFileAttachDtoTemp = new InvFileAttachDTO();
//            inspFileAttachDtoTemp.setDocumentName(fileName);
//            inspFileAttachDtoTemp.setBase64toFile(encoded);
//            inspFileAttachDtoTemp.setFileSave(encoded.getBytes());
//            lstResult.add(inspFileAttachDtoTemp);
//        } catch (Exception e) {
//        }
//        return  lstResult;
        return null;

    }

    private static InputStream getInputStreamByFileName(String fileName) {
        try {
            if (Objects.nonNull(fileName) && fileName.contains(".")) {
                String sysPath = System.getProperty("user.dir").replace("\\", "/");
                String templatePath = sysPath+ "/"+ fileName;
                LOGGER.info("InputStream--> "+templatePath);
                InputStream inputStream = new FileInputStream(templatePath);
                return inputStream;
            } else {
                return null;
            }
        } catch (IOException ioE) {
            return null;
        }
    }

    private static String getFullPathFileNameReport(String preName) {
        String sysPath = System.getProperty("user.dir").replace("\\", "/");
        String pattern = "yyyyMMdd_HHmmss";
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime now = LocalDateTime.now();
        return sysPath + File.separator + preName + "_" + dtf.format(now) + ".xlsx";
    }

}
