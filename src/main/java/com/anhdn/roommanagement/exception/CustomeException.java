package com.anhdn.roommanagement.exception;


import org.springframework.beans.factory.annotation.Autowired;

public class CustomeException extends  RuntimeException{

    @Autowired
    private ErrorMessageLoader errorMessageLoader;
    private ExceptionResponse response;

    public CustomeException(String code) {
        super(code);
        this.response = new ExceptionResponse(code,errorMessageLoader.getMessage(code));
    }

    public ExceptionResponse getResponse() {
        return response;
    }
}
