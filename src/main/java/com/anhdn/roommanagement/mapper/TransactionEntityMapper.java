package com.anhdn.roommanagement.mapper;

import com.anhdn.roommanagement.entity.TransactionEntity;
import com.anhdn.roommanagement.model.request.TransactionRequest;

import java.util.Objects;

public class TransactionEntityMapper {

    public static TransactionEntity transactionEntityMapper(TransactionEntity transactionEntity, TransactionRequest transactionRequest ){

        if(Objects.isNull(transactionEntity)){
            transactionEntity = new TransactionEntity();
        }else {
            transactionEntity.setId(transactionRequest.getId());
        }
        transactionEntity.setCustomerId(transactionRequest.getCustomerId());
        transactionEntity.setDeposits(transactionRequest.getDeposits());
        transactionEntity.setUserId(transactionRequest.getUserId());
        transactionEntity.setStatus(transactionRequest.getStatus());
        transactionEntity.setRoomId(transactionRequest.getRoomId());
        transactionEntity.setCreatedAt(transactionRequest.getCreatedAt());
        transactionEntity.setExpiredAt(transactionRequest.getExpiredAt());
        transactionEntity.setUpdatedAt(transactionRequest.getUpdatedAt());

        return transactionEntity;
    }

    public static TransactionEntity transactionEntityMapper(TransactionEntity transactionEntity){

        transactionEntity.setId(transactionEntity.getId());
        transactionEntity.setCustomerId(transactionEntity.getCustomerId());
        transactionEntity.setDeposits(transactionEntity.getDeposits());
        transactionEntity.setUserId(transactionEntity.getUserId());
        transactionEntity.setStatus(transactionEntity.getStatus());
        transactionEntity.setRoomId(transactionEntity.getRoomId());
        transactionEntity.setCreatedAt(transactionEntity.getCreatedAt());
        transactionEntity.setExpiredAt(transactionEntity.getExpiredAt());
        transactionEntity.setUpdatedAt(transactionEntity.getUpdatedAt());

        return transactionEntity;
    }
}
