package com.anhdn.roommanagement.mapper;

import com.anhdn.roommanagement.entity.UserEntity;
import com.anhdn.roommanagement.model.request.UserRequest;

import java.util.Objects;

public class UserEntityMapper {

    public static UserEntity userEntityMapper(UserEntity userEntity ,UserRequest userRequest){
        if(Objects.isNull(userEntity)){
            userEntity = new UserEntity();
        }else {
            userEntity.setId(userRequest.getId());
        }
        userEntity.setPhone(userRequest.getPhone());
        userEntity.setFullName(userRequest.getFullName());
        userEntity.setUsername(userRequest.getUsername());
        userEntity.setIcNumber(userRequest.getIcNumber());
        userEntity.setIcType(userRequest.getIcType());
        userEntity.setAddressDetail(userRequest.getAddressDetail());
        userEntity.setStatus(userRequest.getStatus());
        userEntity.setCreatedAt(userRequest.getCreatedAt());
        return userEntity;
    }
}
