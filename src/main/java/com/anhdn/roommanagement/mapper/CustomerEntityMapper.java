package com.anhdn.roommanagement.mapper;

import com.anhdn.roommanagement.entity.CustomerEntity;
import com.anhdn.roommanagement.model.request.CustomerRequest;

import java.util.Objects;

public class CustomerEntityMapper {

    public static CustomerEntity customerEntityMapper(CustomerEntity customerEntity, CustomerRequest customerRequest){
        if(Objects.isNull(customerEntity)){
            customerEntity = new CustomerEntity();
        }else {
            customerEntity.setId(customerRequest.getId());
        }
        customerEntity.setPhone(customerRequest.getPhone());
        customerEntity.setFullName(customerRequest.getFullName());
        customerEntity.setIcNumber(customerRequest.getIcNumber());
        customerEntity.setIcType(customerRequest.getIcType());
        customerEntity.setAddressDetail(customerRequest.getAddressDetail());
        customerEntity.setStatus(customerRequest.getStatus());
        customerEntity.setCreatedAt(customerRequest.getCreatedAt());
        return customerEntity;
    }
}
