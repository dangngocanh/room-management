package com.anhdn.roommanagement.mapper;

import com.anhdn.roommanagement.entity.RoomEntity;
import com.anhdn.roommanagement.model.request.RoomRequest;

import java.util.Objects;

public class RoomEntityMapper {

    public static RoomEntity roomEntityMapper(RoomEntity roomEntity, RoomRequest roomRequest){
        if(Objects.isNull(roomEntity)) {
            roomEntity = new RoomEntity();
        }else {
            roomEntity.setId(roomRequest.getId());
        }
        roomEntity.setAddressDetail(roomRequest.getAddressDetail());
        roomEntity.setArea(roomRequest.getArea());
        roomEntity.setCity(roomRequest.getCity());
        roomEntity.setCityCode(roomRequest.getCityCode());
        roomEntity.setCreatedAt(roomRequest.getCreatedAt());
        roomEntity.setWaterPrice(roomRequest.getWaterPrice());
        roomEntity.setUserId(roomRequest.getUserId());
        roomEntity.setStatus(roomRequest.getStatus());
        roomEntity.setRoomNumber(roomRequest.getRoomNumber());
        roomEntity.setDistrict(roomRequest.getDistrict());
        roomEntity.setDistrictCode(roomRequest.getDistrictCode());
        roomEntity.setElectricityPrice(roomRequest.getElectricityPrice());
        roomEntity.setPrice(roomRequest.getPrice());

        return roomEntity;
    }
}
