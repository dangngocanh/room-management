package com.anhdn.roommanagement.validate;

import com.anhdn.roommanagement.model.request.UserRequest;

import java.util.Objects;

public class UserValidate {

    public static Boolean checkValidateUser(UserRequest userRequest){
        if(Objects.isNull(userRequest) || userRequest.getUsername() == null){
            return false;
        }
        return true;
    }
}
