package com.anhdn.roommanagement.service;

import com.anhdn.roommanagement.entity.UserEntity;
import com.anhdn.roommanagement.model.request.UserRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {
    Page<UserEntity> searchListUsername(UserRequest userRequest);
    UserEntity upsert(UserRequest userRequest);
    Boolean deleteById(Long id);
    List<UserEntity> findAll();
}
