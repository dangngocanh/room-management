package com.anhdn.roommanagement.service;

import com.anhdn.roommanagement.entity.CustomerEntity;
import com.anhdn.roommanagement.model.request.CustomerRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CustomerService {

    Page<CustomerEntity> search(CustomerRequest customerRequest);
    CustomerEntity upsert(CustomerRequest customerRequest);
    Boolean deleteById(Long id);
    List<CustomerEntity> getListCustomer();

}
