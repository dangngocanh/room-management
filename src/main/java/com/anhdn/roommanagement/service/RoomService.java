package com.anhdn.roommanagement.service;

import com.anhdn.roommanagement.entity.RoomEntity;
import com.anhdn.roommanagement.model.request.RoomRequest;
import org.springframework.data.domain.Page;

public interface RoomService {
    Page<RoomEntity> searchRoom(RoomRequest roomRequest);
    RoomEntity upsert(RoomRequest roomRequest);
    Boolean deleteId(Long id);
    Page<RoomEntity> checkRoom(RoomRequest roomRequest);
}
