package com.anhdn.roommanagement.service;

import com.anhdn.roommanagement.entity.CustomerEntity;
import com.anhdn.roommanagement.mapper.CustomerEntityMapper;
import com.anhdn.roommanagement.model.request.CustomerRequest;
import com.anhdn.roommanagement.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Override
    public Page<CustomerEntity> search(CustomerRequest customerRequest) {
        Pageable pageable = PageRequest.of(customerRequest.getPage()-1, customerRequest.getPageSize(), Sort.by("id").ascending());
        Page<CustomerEntity> customerEntityPage = customerRepository.search(customerRequest.getFullName(), pageable);
        return customerEntityPage;
    }

    @Override
    public CustomerEntity upsert(CustomerRequest customerRequest) {
        CustomerEntity customerEntity = new CustomerEntity();
        if(customerRequest.getId() == null){
            customerEntity = CustomerEntityMapper.customerEntityMapper(customerEntity, customerRequest);
            return customerRepository.save(customerEntity);
        }
        Optional<CustomerEntity> customerEntity1 = customerRepository.findById(customerRequest.getId());

        if(!customerEntity1.isPresent()){
            throw new RuntimeException("Not found id Customer");
        }
        customerEntity = CustomerEntityMapper.customerEntityMapper(customerEntity1.get(), customerRequest);
        return customerRepository.save(customerEntity);
    }

    @Override
    public Boolean deleteById(Long id) {
        Optional<CustomerEntity> customerEntity = customerRepository.findById(id);
        Boolean checkDelete = false;
        if(customerEntity.isPresent()){
            customerRepository.deleteById(id);
            checkDelete = true;
        }
        return checkDelete;
    }

    @Override
    public List<CustomerEntity> getListCustomer() {
        customerRepository.getListCustomer();
        return customerRepository.getListCustomer();
    }
}
