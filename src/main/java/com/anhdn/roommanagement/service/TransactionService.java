package com.anhdn.roommanagement.service;

import com.anhdn.roommanagement.entity.TransactionEntity;
import com.anhdn.roommanagement.model.request.ReportRequest;
import com.anhdn.roommanagement.model.request.TransactionRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TransactionService {
    Page<TransactionEntity> search(TransactionRequest transactionRequest);
    TransactionEntity upsert(TransactionRequest transactionRequest);
    Boolean deleteById(Long id);
    List<TransactionEntity> finDateBetween(ReportRequest reportRequest);

}
