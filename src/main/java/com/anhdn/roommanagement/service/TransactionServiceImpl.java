package com.anhdn.roommanagement.service;

import com.anhdn.roommanagement.entity.*;
import com.anhdn.roommanagement.mapper.TransactionEntityMapper;
import com.anhdn.roommanagement.model.request.ReportRequest;
import com.anhdn.roommanagement.model.request.TransactionRequest;
import com.anhdn.roommanagement.repositories.CustomerRepository;
import com.anhdn.roommanagement.repositories.RoomRepository;
import com.anhdn.roommanagement.repositories.TransactionDetailRepository;
import com.anhdn.roommanagement.repositories.TransactionRepository;
import net.sf.jxls.transformer.Workbook;
import net.sf.jxls.transformer.XLSTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Service

public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionDetailRepository transactionDetailRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public Page<TransactionEntity> search(TransactionRequest transactionRequest) {
        return null;
    }

    @Override
    @Transactional
    public TransactionEntity upsert(TransactionRequest transactionRequest) {
        //create
        TransactionEntity transactionEntity = new TransactionEntity();
        if(transactionRequest.getId()== null){
            Optional<CustomerEntity> customerEntity = customerRepository.findById(Long.parseLong(transactionRequest.getCustomerId()));
            if(!customerEntity.isPresent()){
                throw new RuntimeException("Not Found id customer");
            }
            //Check room còn trống không
            RoomEntity roomEntity = roomRepository.findByIdAndStatus(Long.parseLong(transactionRequest.getRoomId()),"0");

            if(Objects.nonNull(roomEntity)){
                roomEntity.setStatus("1");
                roomRepository.save(roomEntity);
            }else {
                throw new RuntimeException("Room isn't empty");
            }
            transactionEntity = TransactionEntityMapper.transactionEntityMapper(transactionEntity , transactionRequest);
            transactionEntity.setStatus("1"); // 0 là đang trống, 1 // đã sử dụng
            transactionEntity.setCreatedAt(new Date());
            transactionEntity = transactionRepository.save(transactionEntity);


            // Lưu thông tin chi tiết giao dịch
            TransactionDetailEntity transactionDetailEntity = new TransactionDetailEntity();
            transactionDetailEntity.setTransactionId(transactionEntity.getCustomerId());
            transactionDetailEntity.setTotalPrice(roomEntity.getPrice());
            transactionDetailEntity.setCreatedAt(new Date());
            transactionDetailRepository.save(transactionDetailEntity);

//            transactionDetailRepository.sa
            return transactionEntity;
        }
        // Màn sửa
        Optional<TransactionEntity> transactionEntity1 = transactionRepository.findById(transactionRequest.getId());
        if(!transactionEntity1.isPresent()){
            throw new RuntimeException("Not found id Transaction");
        }
        // update date
        transactionEntity = TransactionEntityMapper.transactionEntityMapper(transactionEntity1.get());
        transactionEntity.setUpdatedAt(new Date());
        return transactionRepository.save(transactionEntity);
    }

    @Override
    public Boolean deleteById(Long id) {
        Optional<TransactionEntity> transactionEntity = transactionRepository.findById(id);
        Boolean checkDelete = false;
        if(transactionEntity.isPresent()){
            transactionRepository.deleteById(id);
            checkDelete = true;
        }
        return checkDelete;
    }

    @Override
    public List<TransactionEntity> finDateBetween(ReportRequest reportRequest) {
        String dateTo = reportRequest.getDateTo() + " 00:00:00";
        String dateEnd = reportRequest.getDateEnd() + " 23:59:00";
        SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            Date dateFormatTo = simpleDateFormat.parse(dateTo);
            Date dateFormatEnd = simpleDateFormat.parse(dateEnd);
            List<TransactionEntity> transactionEntities = transactionRepository.findByCreatedAtBetween(dateFormatTo, dateFormatEnd);

            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            String filePath = classloader.getResource("../" + "doc-template/ims").getPath();


            String template;
            template = "DANH_DACH_YCCG" + ".xlsx";
            String dateFileTemp = new SimpleDateFormat("yyMMdd_hhmmss").format(new Date());
            try (InputStream in = new FileInputStream(filePath + template)) {
                Map beans = new HashMap();
                File file;


                beans.put("results", transactionEntities);
                file = File.createTempFile("DANH_SACH_YCCG_" + dateFileTemp + "_", ".xlsx");
                XLSTransformer transformer = new XLSTransformer();
                 transformer.transformXLS(in, beans);
//                Workbook resultWorkbook =
//
//                try (OutputStream bos = new FileOutputStream(file)) {
//                    resultWorkbook.write(bos);
//                    FileInfoDTO fileInfoDto = new FileInfoDTO();
//                    fileInfoDto.setReportFilePathEncrypt(UEncrypt.encryptFileUploadPath(file.getName()));
//                    fileInfoDto.setFileName(file.getName());
//                    return fileInfoDto;
//                }
            }



            return null;
        }catch (Exception e){
            System.out.println(e);
        }


        return null;
    }
}
