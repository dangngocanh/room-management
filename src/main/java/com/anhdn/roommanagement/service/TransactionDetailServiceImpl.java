package com.anhdn.roommanagement.service;

import com.anhdn.roommanagement.entity.RoomEntity;
import com.anhdn.roommanagement.entity.TransactionDetailEntity;
import com.anhdn.roommanagement.entity.TransactionEntity;
import com.anhdn.roommanagement.model.request.TransactionDetailRequest;
import com.anhdn.roommanagement.repositories.RoomRepository;
import com.anhdn.roommanagement.repositories.TransactionDetailRepository;
import com.anhdn.roommanagement.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@Service
public class TransactionDetailServiceImpl implements TransactionDetailService {

    @Autowired
    private TransactionDetailRepository transactionDetailRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public TransactionDetailEntity upsert(TransactionDetailRequest transactionDetailRequest) {
        TransactionDetailEntity transactionDetailEntity = new TransactionDetailEntity();
        if(transactionDetailRequest.getId() == null){
            Optional<TransactionEntity> transactionEntity = transactionRepository.findById(Long.parseLong(transactionDetailRequest.getTransactionId()));
            if(!transactionEntity.isPresent()){
                throw new RuntimeException("Not found Transaction id");
            }

            Optional<RoomEntity> roomEntity = roomRepository.findById(Long.parseLong(transactionEntity.get().getRoomId()));
            RoomEntity roomEntity1 = roomEntity.get();
            if(Integer.parseInt(transactionDetailRequest.getElectricNumber()) < Integer.parseInt(roomEntity1.getElectricityCurrent())){
                throw new RuntimeException("Validate ElectricNumber not < getElectricityCurrent ");
            }
            if(Integer.parseInt(transactionDetailRequest.getWaterNumber()) < Integer.parseInt(roomEntity1.getWaterCurrent())){
                throw new RuntimeException("Validate getWaterNumber not < getWaterCurrent ");
            }
            int getNumberElectricity =  Integer.parseInt(transactionDetailRequest.getElectricNumber()) - Integer.parseInt(roomEntity1.getElectricityCurrent());
            int getNumberWater =  Integer.parseInt(transactionDetailRequest.getWaterNumber()) - Integer.parseInt(roomEntity1.getWaterCurrent());

            transactionDetailEntity.setCreatedAt(new Date());
            int totalMoney = Integer.parseInt(roomEntity1.getPrice()) +
                    (getNumberElectricity * Integer.parseInt(roomEntity1.getElectricityPrice())) +
                    (getNumberWater * Integer.parseInt(roomEntity1.getWaterPrice()));

            transactionDetailEntity.setTotalPrice(String.valueOf(totalMoney));
            transactionDetailEntity = transactionDetailRepository.save(transactionDetailEntity);

            roomEntity1.setElectricityCurrent(transactionDetailRequest.getElectricNumber());
            roomEntity1.setWaterCurrent(transactionDetailRequest.getWaterNumber());
            roomRepository.save(roomEntity1);
            return transactionDetailEntity;

        }


        Optional<TransactionDetailEntity> transactionDetailEntity1 = transactionDetailRepository.findById(transactionDetailRequest.getId());



        transactionDetailEntity = transactionDetailEntity1.get();
//        transactionDetailEntity.setCreatedAt();
        return transactionDetailEntity;
    }
}
