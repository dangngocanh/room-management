package com.anhdn.roommanagement.service;

import com.anhdn.roommanagement.entity.TransactionDetailEntity;
import com.anhdn.roommanagement.model.request.TransactionDetailRequest;

public interface TransactionDetailService {

    TransactionDetailEntity upsert(TransactionDetailRequest transactionDetailRequest);

}
