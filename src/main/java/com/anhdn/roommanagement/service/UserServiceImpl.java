package com.anhdn.roommanagement.service;

import com.anhdn.roommanagement.entity.RoomEntity;
import com.anhdn.roommanagement.entity.UserEntity;
import com.anhdn.roommanagement.mapper.UserEntityMapper;
import com.anhdn.roommanagement.model.request.UserRequest;
import com.anhdn.roommanagement.repositories.UserRepository;
import com.anhdn.roommanagement.validate.UserValidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Page<UserEntity> searchListUsername(UserRequest userRequest) {
        Pageable pageable = PageRequest.of(userRequest.getPage()-1,userRequest.getPageSize(), Sort.by("id").ascending());
//        Page<UserEntity> userEntityPage = userRepository.search(userRequest.getUsername(), userRequest.getFullName(), pageable);
        Page<UserEntity> userEntityPage = userRepository.searchUser(userRequest.getUsername(), userRequest.getFullName(), pageable);
        return userEntityPage;
    }

    @Override
    public UserEntity upsert(UserRequest userRequest) {
        Boolean checkUserValidate =  UserValidate.checkValidateUser(userRequest);
        if(checkUserValidate == false){
            throw new RuntimeException("User is validate");
        }
        UserEntity userEntity = new UserEntity();

        if(Objects.nonNull(userRequest) && userRequest.getId() == null){
            //create
            List<UserEntity> userEntities = userRepository.findByUsername(userRequest.getUsername());
            if(Objects.nonNull(userEntities) && userEntities.size() >0){
                throw new RuntimeException(" User isn't  duplicate");
            }else {
                userEntity = UserEntityMapper.userEntityMapper(userEntity, userRequest);
                return userRepository.save(userEntity);
            }
        }
        Optional<UserEntity>  getIdUser = userRepository.findById(userRequest.getId());
        if(!getIdUser.isPresent()){
            throw new RuntimeException(" Error not found user");
        }

        userEntity = UserEntityMapper.userEntityMapper(getIdUser.get(), userRequest);
        return userRepository.save(userEntity);
    }

    @Override
    public Boolean deleteById(Long id) {
        Optional<UserEntity> userEntity = userRepository.findById(id);
        Boolean checkDelete = false;
        if(userEntity.isPresent()){
            userRepository.deleteById(id);
            checkDelete = true;
        }
        return checkDelete;
    }

    @Override
    public List<UserEntity> findAll() {
        return userRepository.findAll();
    }
}
