package com.anhdn.roommanagement.service;

import com.anhdn.roommanagement.entity.RoomEntity;
import com.anhdn.roommanagement.mapper.RoomEntityMapper;
import com.anhdn.roommanagement.model.request.RoomRequest;
import com.anhdn.roommanagement.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public Page<RoomEntity> searchRoom(RoomRequest roomRequest) {
        Pageable pageable = PageRequest.of(roomRequest.getPage()-1,roomRequest.getPageSize(), Sort.by("id").ascending());
        Page<RoomEntity> roomEntityPage = roomRepository.search(roomRequest.getRoomNumber(), pageable);
        return roomEntityPage;
    }

    @Override
    public RoomEntity upsert(RoomRequest roomRequest) {
        RoomEntity roomEntity = new RoomEntity();
        if(roomRequest.getId() == null){
            roomEntity = RoomEntityMapper.roomEntityMapper(roomEntity, roomRequest);
            return roomRepository.save(roomEntity);
        }
        Optional<RoomEntity> roomEntity2 = roomRepository.findById(roomRequest.getId());
        if(!roomEntity2.isPresent()){
            throw new RuntimeException(" Error not found Room");
        }
        roomEntity = RoomEntityMapper.roomEntityMapper(roomEntity2.get(), roomRequest);
        return roomRepository.save(roomEntity);
    }

    @Override
    public Boolean deleteId(Long id) {
        Optional<RoomEntity> roomEntity = roomRepository.findById(id);
        Boolean checkDelete = false;
        if(roomEntity.isPresent()){
            roomRepository.deleteById(id);
            checkDelete = true;
        }
        return checkDelete;
    }

    @Override
    public Page<RoomEntity> checkRoom(RoomRequest roomRequest) {
        Pageable pageable = PageRequest.of(roomRequest.getPage()-1,roomRequest.getPageSize(), Sort.by("id").ascending());
        Page<RoomEntity> roomEntityPage = roomRepository.search(roomRequest.getStatus(), pageable);
        return roomEntityPage;
    }
}
