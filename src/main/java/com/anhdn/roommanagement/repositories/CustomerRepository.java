package com.anhdn.roommanagement.repositories;

import com.anhdn.roommanagement.entity.CustomerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

    @Query("select u from CustomerEntity u where instr(lower(u.fullName), ?1) > 0 ")
    Page<CustomerEntity> search(String fullName, Pageable page);

    @Procedure(name="getAllCustomers")
//    @Query(value = "CALL `roommanagement`.GetAllCustomer();", nativeQuery = true)
    List<CustomerEntity> getListCustomer();

}
