package com.anhdn.roommanagement.repositories;

import com.anhdn.roommanagement.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query("select u from UserEntity u where instr(lower(u.username), ?1) > 0 or instr(lower(u.fullName), ?2) > 0")
    Page<UserEntity> search(String username, String fullName, Pageable pageable);

    @Query("select u from UserEntity u where u.username like %:username% or u.fullName like %:fullName%")
    Page<UserEntity> searchUser(@Param("username") String username, @Param("fullName") String fullName, Pageable pageable);

    List<UserEntity> findByUsername(String username);

    Optional<UserEntity> findById(Long id);

    List<UserEntity> findAll();
}
