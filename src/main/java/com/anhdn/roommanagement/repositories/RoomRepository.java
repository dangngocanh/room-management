package com.anhdn.roommanagement.repositories;

import com.anhdn.roommanagement.entity.RoomEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<RoomEntity,Long> {
    @Query("select u from RoomEntity u where instr(lower(u.roomNumber), ?1)> 0 ")
    Page<RoomEntity> search(String roomNumber, Pageable pageable);

    Page<RoomEntity> findByStatus(Long status, Pageable pageable);

    RoomEntity findByIdAndStatus(Long id, String status);
}
