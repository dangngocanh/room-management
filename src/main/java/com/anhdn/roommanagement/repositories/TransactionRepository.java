package com.anhdn.roommanagement.repositories;

import com.anhdn.roommanagement.entity.TransactionEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Long> {
    Page<TransactionEntity> findAll(Pageable pageable);

    List<TransactionEntity> findByCreatedAtBetween(Date to, Date end);
    List<TransactionEntity> findByCreatedAtLessThanEqualAndCreatedAtGreaterThan(Date to, Date end);
}
