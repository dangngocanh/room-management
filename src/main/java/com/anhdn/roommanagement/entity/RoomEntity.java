package com.anhdn.roommanagement.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name="room")
public class RoomEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="room_number")
    private String roomNumber;

    @Column(name="city")
    private String city;

    @Column(name="city_code")
    private String cityCode;

    @Column(name="district")
    private String district;

    @Column(name="district_code")
    private String districtCode;

    @Column(name="user_id")
    private String userId;

    @Column(name="price")
    private String price;

    @Column(name="address_detail")
    private String addressDetail;

    @Column(name="area") // diện tích
    private String area;

    @Column(name="status")
    private String status;

    @Column(name="created_at")
    private Date createdAt;

    @Column(name="electricity_price")
    private String electricityPrice;

    @Column(name="water_price")
    private String waterPrice;

    @Column(name="electricity_current")
    private String electricityCurrent;

    @Column(name="water_current")
    private String waterCurrent;



}
