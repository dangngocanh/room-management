package com.anhdn.roommanagement.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="user")
@Data
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="phone")
    private String phone;

    @Column(name="full_name")
    private String fullName;

    @Column(name="username")
    private String username;

    @Column(name="status")
    private String status;

    @Column(name="address_detail")
    private String addressDetail;

    @Column(name="ic_number")
    private String icNumber;

    @Column(name="ic_type")
    private String icType;

    @Column(name="created_at")
    private Date createdAt;
}
