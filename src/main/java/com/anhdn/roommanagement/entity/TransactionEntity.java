package com.anhdn.roommanagement.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name="transaction")
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="customer_id")
    private String customerId;

    @Column(name="deposits")
    private String deposits;

    @Column(name="room_id")
    private String roomId;

    @Column(name="status")
    private String status;

    @Column(name="user_id")
    private String userId;

    @Column(name="created_at")
    private Date createdAt;

    @Column(name="updated_at")
    private Date updatedAt;

    @Column(name="expired_at")
    private Date expiredAt;

}
