package com.anhdn.roommanagement.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name="transaction_detail")
public class TransactionDetailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="transaction_id")
    private String transactionId;

    @Column(name="electric_number")
    private String electricNumber;

    @Column(name="water_number")
    private String waterNumber;

    @Column(name="total_price")
    private String totalPrice;

    @Column(name="created_at")
    private Date createdAt;

    @Column(name="updated_at")
    private Date updatedAt;
}
